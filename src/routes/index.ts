import type { RequestHandler } from "@sveltejs/kit/types/internal";
import { api } from "./_api";

export const get: RequestHandler = async ({ params }) => {
  // locals.userid comes from src/hooks.js
  const response = await api("get", `playlists/all`);

  if (response.status === 200) {
    return {
      body: {
        allPlaylists: await response.json(),
      },
    };
  }

  return {
    status: response.status,
  };
};
