import { writable } from "svelte/store";
export const secondaryPaneContent = writable([{}]);
export const secondaryPaneToggled = writable(false);
